# encoding: UTF-8
# 不用ifconfig命令后获取，解决某些内网IP可能不是192.168.开头的问题，转自 http://stackoverflow.com/questions/5029427/ruby-get-local-ip-nix/7809304#7809304

require 'socket'
puts Socket.ip_address_list.detect{|intf| intf.ipv4_private?}.ip_address # => 192.168.1.104